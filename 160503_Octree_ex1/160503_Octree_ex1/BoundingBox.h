#ifndef ___BOUNDINGBOX___HHH___
#define ___BOUNDINGBOX___HHH___

#include <float.h>
#include <vector>
#include <Windows.h>
#include "Vertex.h"
#include "Vector3.h"

class BoundingBox
{
public:
	Vertex m_vMax, m_vMin;
	
	BoundingBox(Vertex * pVerts, int numVerts);
	BoundingBox();
	~BoundingBox();
	
	void makeBoundingBox(Vertex * pVerts, int numVerts);
	Vector3 getBBcentroid(void);
	bool isInside(Vector3 p);
	
	Vector3 m_v3Result;
	void CopyBB(BoundingBox BB);
	void CopyBB(BoundingBox *pBB);
};
#endif // !___BOUNDINGBOX___HHH___