#ifndef ___OT_NODE___HHH___
#define ___OT_NODE___HHH___
#define MAX_LEVEL 8

#include <stdio.h>
#include "HScommon.h"
#include "Mesh.h"
#include "Triangle.h"

class OT_Node 
{
public:
	OT_Node **			m_ppChildNodes;
	unsigned char		m_ucLevel;
	static Mesh *		m_pMesh;
	BoundingBox			m_BB;

	std::vector<unsigned int> m_vecTriangleID;

	OT_Node();
	OT_Node(Mesh * pMesh);
	~OT_Node();

	void dividedNode(OT_Node *pN);
	/*
	static void MyLight();
	static void display(void);
	static void DoKeyboard(unsigned char key, int x, int y);
	*/
};

#endif // !___OT_NODE___HHH___