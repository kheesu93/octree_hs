#include "Octree.h"


Octree::Octree()
{
	m_pRoot = NULL;
}

Octree::~Octree()
{
	SAFEDELETE(m_pRoot);
}

Octree::Octree(Mesh * pMesh)	// only for root node.
{
	if (NULL == pMesh)
		return;

	m_pRoot = new OT_Node(pMesh);
	m_pRoot->m_BB.CopyBB(pMesh->m_pBB);
	m_pRoot->dividedNode(m_pRoot);
}
void Octree::draw()
{
	BreadthTrav(m_pRoot);
}

void Octree::BreadthTrav(OT_Node * pO)
{
	Vertex * pvMax = &pO->m_BB.m_vMax;
	Vertex * pvMin = &pO->m_BB.m_vMin;
	Vector3 * pvBBcentroid = &pO->m_BB.getBBcentroid();

	float lx = pvMax->x - pvMin->x;
	float ly = pvMax->y - pvMin->y;
	float lz = pvMax->z - pvMin->z;
	float lmax = max(max(lx, ly), lz);

	glScalef(1.0f / lmax, 1.0f / lmax, 1.0f / lmax);
	glTranslatef(pvBBcentroid->x, pvBBcentroid->y, pvBBcentroid->z);
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	for (int Tindex = 0; Tindex < pO->m_pMesh->m_numFaces; Tindex++) {
		Triangle * pT = &pO->m_pMesh->m_pFaces[Tindex];
		glVertex3f(pO->m_pMesh->m_pVertices[pT->m_v[0]].x, pO->m_pMesh->m_pVertices[pT->m_v[0]].y, pO->m_pMesh->m_pVertices[pT->m_v[0]].z);
		glVertex3f(pO->m_pMesh->m_pVertices[pT->m_v[1]].x, pO->m_pMesh->m_pVertices[pT->m_v[1]].y, pO->m_pMesh->m_pVertices[pT->m_v[1]].z);
		glVertex3f(pO->m_pMesh->m_pVertices[pT->m_v[1]].x, pO->m_pMesh->m_pVertices[pT->m_v[1]].y, pO->m_pMesh->m_pVertices[pT->m_v[1]].z);
		glVertex3f(pO->m_pMesh->m_pVertices[pT->m_v[2]].x, pO->m_pMesh->m_pVertices[pT->m_v[2]].y, pO->m_pMesh->m_pVertices[pT->m_v[2]].z);
		glVertex3f(pO->m_pMesh->m_pVertices[pT->m_v[2]].x, pO->m_pMesh->m_pVertices[pT->m_v[2]].y, pO->m_pMesh->m_pVertices[pT->m_v[2]].z);
		glVertex3f(pO->m_pMesh->m_pVertices[pT->m_v[0]].x, pO->m_pMesh->m_pVertices[pT->m_v[0]].y, pO->m_pMesh->m_pVertices[pT->m_v[0]].z);
	}
	glEnd();

	Queue Q;
	Q.Enqueue(pO);

	while (!Q.empty())
	{
		pO = Q.dequeue();	
		Vertex * vMax = &pO->m_BB.m_vMax;
		Vertex * vMin = &pO->m_BB.m_vMin;
		
		glColor3f(1.0f, 1.0f, 0.0f);
		glBegin(GL_LINE_LOOP);
		glVertex3f(vMin->x, vMin->y, vMin->z);	//0
		glVertex3f(vMax->x, vMin->y, vMin->z);	//1
		glVertex3f(vMax->x, vMax->y, vMin->z);	//3
		glVertex3f(vMin->x, vMax->y, vMin->z);	//2

		glVertex3f(vMin->x, vMin->y, vMin->z);	//0
		glVertex3f(vMin->x, vMax->y, vMin->z);	//2
		glVertex3f(vMin->x, vMax->y, vMax->z);	//6
		glVertex3f(vMin->x, vMin->y, vMax->z);	//4

		glVertex3f(vMin->x, vMin->y, vMax->z);	//4
		glVertex3f(vMax->x, vMin->y, vMax->z);	//5
		glVertex3f(vMax->x, vMax->y, vMax->z);	//7
		glVertex3f(vMin->x, vMax->y, vMax->z);	//6

		glVertex3f(vMin->x, vMax->y, vMax->z);	//6
		glVertex3f(vMin->x, vMax->y, vMin->z);	//2
		glVertex3f(vMax->x, vMax->y, vMin->z);	//3
		glVertex3f(vMax->x, vMax->y, vMax->z);	//7

		glVertex3f(vMax->x, vMax->y, vMax->z);	//7
		glVertex3f(vMax->x, vMax->y, vMin->z);	//3
		glVertex3f(vMax->x, vMin->y, vMin->z);	//1
		glVertex3f(vMax->x, vMin->y, vMax->z);	//5

		glVertex3f(vMax->x, vMin->y, vMax->z);	//5
		glVertex3f(vMin->x, vMin->y, vMax->z);	//4
		glVertex3f(vMin->x, vMin->y, vMin->z);	//0
		glVertex3f(vMax->x, vMin->y, vMin->z);	//1
		glEnd();
		
		for (int i = 0; i < 8; i++)
		{
			if (pO->m_ppChildNodes) 
				Q.Enqueue(pO->m_ppChildNodes[i]);
		}
	}
}