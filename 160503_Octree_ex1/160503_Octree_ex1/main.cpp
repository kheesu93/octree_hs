#include <stdio.h>
#include <gl/glut.h>
#include "Octree.h"

Mesh  * g_ptriMesh;
Octree * g_pmeshTree;

void reshape(int w, int h)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, w, h);
	//glOrtho(-1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f);
	gluPerspective(60, (GLfloat)w / (GLfloat)h, 1.0, 100.0);
}

float glMoveX, glMoveY;
float glXAngle = 90.0f;
float glYAngle = 0.0f;
float glZAngle = 180.0f;
float glSize = 1.0f;

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glShadeModel(GL_SMOOTH);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0, 0, 1.5, 0, 0, 0, 0, 1, 0);
	//glColor3f(1.0f,1.0f,1.0f);
	
	glRotatef(glXAngle, 1, 0, 0);
	glRotatef(glYAngle, 0, 1, 0);
	glRotatef(glZAngle, 0, 0, 1);
	glScalef(glSize, glSize, glSize);
	glTranslatef(glMoveX, glMoveY, 0.0f);

	g_pmeshTree->draw();

	//glutPostRedisplay();
	glutSwapBuffers();
}

void DoKeyboard(unsigned char key, int x, int y)
{
	switch (key) {
	case 'w': //UP
		glMoveY += 1.0f;  break;
	case 's': //Down
		glMoveY -= 1.0f;  break;
	case 'a': //Left
		glMoveX -= 1.0f;  break;
	case 'd': //Right
		glMoveX += 1.0f;  break;
	case '+': //Scale Up
		glSize += 0.5f;  break;
	case '-': //Scale Down
		glSize -= 0.5f;	 break;
	case 'y': //Rotation X--
		glXAngle -= 10.0f;  break;
	case 'b': //Rotation X++
		glXAngle += 10.0f;  break;
	case 'g': //Rotation Y--
		glYAngle -= 10.0f;  break;
	case 'h': //Rotation Y++
		glYAngle += 10.0f;  break;
	case 'n': //Rotation Z--
		glZAngle -= 10.0f;  break;
	case 'm': //Rotation Z++
		glZAngle += 10.0f;  break;
	}
	glutPostRedisplay();
}

void main(int argc, char ** argv)
{
	g_ptriMesh = new Mesh();
	g_ptriMesh->setDebug(false);
	g_ptriMesh->meshRead();

	g_pmeshTree = new Octree(g_ptriMesh);

	glutInit(&argc, argv);
	glutInitWindowSize(600, 600);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutCreateWindow("example");
	glClearColor(0.0,0.0,0.0,1.0);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(DoKeyboard);
	//MyLight();

	glutDisplayFunc(display);
	glutMainLoop();
	delete g_ptriMesh;
	delete g_pmeshTree;
}
