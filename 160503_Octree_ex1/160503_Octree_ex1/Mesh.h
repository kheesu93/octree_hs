#ifndef ________MESH__HHH___________
#define ________MESH__HHH___________

#include <stdio.h>
#include <string>
#include <vector>
#include <gl/glut.h>

#include "Vector3.h"
#include "Triangle.h"
#include "Vertex.h"
#include "BoundingBox.h"
#include "HScommon.h"

#pragma warning(disable:4996)

class Mesh
{
public:
	int m_numVerts;
	int m_numFaces;
	bool m_debug;
	
	Vertex *m_pVertices;
	Triangle *m_pFaces;
	BoundingBox *m_pBB;

	Mesh();
	~Mesh();

	void meshRead(void);
	Vector3 getTcentroid(unsigned int Tid);

	void debugOut(char * str, char * format);
	void setDebug(bool bflag);

};
#endif // !________MESH__HHH___________
