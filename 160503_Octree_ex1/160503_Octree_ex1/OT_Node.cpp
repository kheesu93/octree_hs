#include "OT_Node.h"

Mesh * OT_Node::m_pMesh = NULL;

OT_Node::OT_Node()
{
	m_ucLevel = 0u;
	m_ppChildNodes = NULL;
}
OT_Node::~OT_Node()
{
	SAFEDELETEARRAY(m_ppChildNodes);
}

OT_Node::OT_Node(Mesh * pMesh)	// only for the root node
{
	m_ucLevel = 0u;
	m_ppChildNodes = NULL;
	m_pMesh = pMesh;

	for (int i = 0; i < pMesh->m_numFaces; i++)
	{
		m_vecTriangleID.push_back(i);
	}

#ifdef __MY_DEBUG__

	FILE *fp = fopen("test1.txt", "w");
	fprintf(fp, "\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
	fprintf(fp, "\n Tindex \t|\t v0 (x,y,z)  \t|\t\t\t  v1 (x,y,z)  \t\t\t|\t\t\t  v2 (x,y,z)  \t\t\t");
	fprintf(fp, "\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
	Triangle * pT;
	Vertex * pV;
	unsigned int Vi;
	for (int Ti = 0; Ti < pMesh->m_numFaces; Ti++)
	{
		pT = &pMesh->m_pFaces[Ti];

		fprintf(fp, "\n  %d \t\t|\t", Ti);
		for (int i = 0; i < 3; i++)
		{
			Vi = pT->m_v[i];
			pV = &pMesh->m_pVertices[Vi];
			fprintf(fp, "(%f, %f, %f) \t|\t", pV->x, pV->y, pV->z);
		}

	}
	fprintf(fp, "\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

	fclose(fp);
#endif
}

void OT_Node:: dividedNode(OT_Node *pN)
{
	if (m_vecTriangleID.size() < 2 || m_ucLevel >= MAX_LEVEL)
		return;	//leaf node�� �ȴ�.
	
	m_ppChildNodes = new OT_Node * [8];
	
	for (int i = 0; i < 8; i++)
	{
		m_ppChildNodes[i] = new OT_Node();
		m_ppChildNodes[i]->m_ucLevel++;
	}
	//printf("\n >> size = %d", pN->m_vecTriangleID.size());
	
	Vertex vMin, vMax;
	vMin.x = pN->m_BB.m_vMin.x;
	vMin.y = pN->m_BB.m_vMin.y;
	vMin.z = pN->m_BB.m_vMin.z;

	vMax.x = pN->m_BB.m_vMax.x;
	vMax.y = pN->m_BB.m_vMax.y;
	vMax.z = pN->m_BB.m_vMax.z;
	
	/////////////////[0]
	m_ppChildNodes[0]->m_BB.m_vMax.x = (vMin.x + vMax.x) / 2.0f;
	m_ppChildNodes[0]->m_BB.m_vMax.y = (vMin.y + vMax.y) / 2.0f;
	m_ppChildNodes[0]->m_BB.m_vMax.z = (vMin.z + vMax.z) / 2.0f;

	m_ppChildNodes[0]->m_BB.m_vMin.x = vMin.x;
	m_ppChildNodes[0]->m_BB.m_vMin.y = vMin.y;
	m_ppChildNodes[0]->m_BB.m_vMin.z = vMin.z;

	/////////////////[1]
	m_ppChildNodes[1]->m_BB.m_vMax.x = vMax.x;
	m_ppChildNodes[1]->m_BB.m_vMax.y = (vMin.y + vMax.y) / 2.0f;
	m_ppChildNodes[1]->m_BB.m_vMax.z = (vMin.z + vMax.z) / 2.0f;

	m_ppChildNodes[1]->m_BB.m_vMin.x = (vMin.x + vMax.x) / 2.0f;
	m_ppChildNodes[1]->m_BB.m_vMin.y = vMin.y;
	m_ppChildNodes[1]->m_BB.m_vMin.z = vMin.z;

	/////////////////[2]
	m_ppChildNodes[2]->m_BB.m_vMax.x = (vMax.x + vMin.x) / 2.0f;
	m_ppChildNodes[2]->m_BB.m_vMax.y = vMax.y;
	m_ppChildNodes[2]->m_BB.m_vMax.z = (vMin.z + vMax.z) / 2.0f;

	m_ppChildNodes[2]->m_BB.m_vMin.x = vMin.x;
	m_ppChildNodes[2]->m_BB.m_vMin.y = (vMin.y + vMax.y) / 2.0f;
	m_ppChildNodes[2]->m_BB.m_vMin.z = vMin.z;

	/////////////////[3]
	m_ppChildNodes[3]->m_BB.m_vMax.x = vMax.x;
	m_ppChildNodes[3]->m_BB.m_vMax.y = vMax.y;
	m_ppChildNodes[3]->m_BB.m_vMax.z = (vMin.z + vMax.z) / 2.0f;

	m_ppChildNodes[3]->m_BB.m_vMin.x = (vMin.x + vMax.x) / 2.0f;
	m_ppChildNodes[3]->m_BB.m_vMin.y = (vMin.y + vMax.y) / 2.0f;
	m_ppChildNodes[3]->m_BB.m_vMin.z = vMin.z;

	/////////////////[4]
	m_ppChildNodes[4]->m_BB.m_vMax.x = (vMin.x + vMax.x) / 2.0f;
	m_ppChildNodes[4]->m_BB.m_vMax.y = (vMin.y + vMax.y) / 2.0f;
	m_ppChildNodes[4]->m_BB.m_vMax.z = vMax.z;

	m_ppChildNodes[4]->m_BB.m_vMin.x = vMin.x;
	m_ppChildNodes[4]->m_BB.m_vMin.y = vMin.y;
	m_ppChildNodes[4]->m_BB.m_vMin.z = (vMin.z + vMax.z) / 2.0f;

	/////////////////[5]
	m_ppChildNodes[5]->m_BB.m_vMax.x = vMax.x;
	m_ppChildNodes[5]->m_BB.m_vMax.y = (vMin.y + vMax.y) / 2.0f;
	m_ppChildNodes[5]->m_BB.m_vMax.z = vMax.z;

	m_ppChildNodes[5]->m_BB.m_vMin.x = (vMin.x + vMax.x) / 2.0f;
	m_ppChildNodes[5]->m_BB.m_vMin.y = vMin.y;
	m_ppChildNodes[5]->m_BB.m_vMin.z = (vMin.z + vMax.z) / 2.0f;

	/////////////////[6]
	m_ppChildNodes[6]->m_BB.m_vMax.x = (vMax.x + vMin.x) / 2.0f;
	m_ppChildNodes[6]->m_BB.m_vMax.y = vMax.y;
	m_ppChildNodes[6]->m_BB.m_vMax.z = vMax.z;

	m_ppChildNodes[6]->m_BB.m_vMin.x = vMin.x;
	m_ppChildNodes[6]->m_BB.m_vMin.y = (vMin.y + vMax.y) / 2.0f;
	m_ppChildNodes[6]->m_BB.m_vMin.z = (vMin.z + vMax.z) / 2.0f;

	/////////////////[7]
	m_ppChildNodes[7]->m_BB.m_vMax.x = vMax.x;
	m_ppChildNodes[7]->m_BB.m_vMax.y = vMax.y;
	m_ppChildNodes[7]->m_BB.m_vMax.z = vMax.z;

	m_ppChildNodes[7]->m_BB.m_vMin.x = (vMin.x + vMax.x) / 2.0f;
	m_ppChildNodes[7]->m_BB.m_vMin.y = (vMin.y + vMax.y) / 2.0f;
	m_ppChildNodes[7]->m_BB.m_vMin.z = (vMin.z + vMax.z) / 2.0f;

	unsigned int Tid;
	for (unsigned int i = 0; i<pN->m_vecTriangleID.size(); i++)
	{
		Tid = pN->m_vecTriangleID[i];
		Vector3 centroid = m_pMesh->getTcentroid(Tid);

		for (int j = 0; j<8; j++)
		{
			if (m_ppChildNodes[j]->m_BB.isInside(centroid))
			{
				m_ppChildNodes[j]->m_vecTriangleID.push_back(Tid);
				
			}
		}
	}
	//for (int i = 0; i < 8; i++)
	//{
	//	printf("\n >>> m_ppChildNode[%d] size = %d", i, m_ppChildNodes[i]->m_vecTriangleID.size());
	//}
	pN->m_vecTriangleID.clear();

	for (int j = 0; j < 8; j++) 
	{
		for (int i = 0; i < 8; i++)
			m_ppChildNodes[j]->dividedNode(m_ppChildNodes[i]);
	}
}

/*
void OT_Node::MyLight()
{
	GLfloat global_ambient[] = { 0.1f, 0.1f, 0.1f, 1.0f };

	GLfloat light0_ambient[] = { 0.5f, 0.4f, 0.3f, 1.0f };
	GLfloat light0_diffuse[] = { 0.5f, 0.4f, 0.3f, 1.0f };
	GLfloat light0_spectular[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	GLfloat light1_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLfloat light1_diffuse[] = { 0.5f, 0.2f, 0.3f, 1.0f };
	GLfloat light1_spectular[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	GLfloat material_ambient[] = { 0.3f, 0.3f, 0.3f, 1.0f };
	GLfloat material_diffuse[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat material_spectular[] = { 0.0f, 0.0f, 1.0f, 1.0f };
	GLfloat material_shiniess[] = { 25.0f };

	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light0_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light0_spectular);

	glLightfv(GL_LIGHT1, GL_AMBIENT, light1_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light1_spectular);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_spectular);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shiniess);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, global_ambient);
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
}

float glMoveX, glMoveY, glXAngle, glYAngle, glZAngle;
float glSize = 2.0f;
int state = 2;

void OT_Node::display(void)
{
	float lx = m_pMesh->m_pBB->m_vMax.x - m_pMesh->m_pBB->m_vMin.x;
	float ly = m_pMesh->m_pBB->m_vMax.y - m_pMesh->m_pBB->m_vMin.y;
	float lz = m_pMesh->m_pBB->m_vMax.z - m_pMesh->m_pBB->m_vMin.z;
	float lmax = max(max(lx, ly), lz);
	
	//MyLight();
	//////////////////////////////////////////////////////////////
	GLfloat LightPosition0[] = { -500.0f, -30.0f, 30.0f, 1.0f };
	GLfloat LightPosition1[] = { 500.0f, -30.0f, 10.0f, 1.0f };
	//////////////////////////////////////////////////////////////

	glClear(GL_COLOR_BUFFER_BIT);
	glShadeModel(GL_SMOOTH);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0, 0, 1.5, 0, 0, 0, 0, 1, 0);

	glRotatef(glXAngle, 1, 0, 0);
	glRotatef(glYAngle, 0, 1, 0);
	glRotatef(glZAngle, 0, 0, 1);
	glScalef(glSize, glSize, glSize);
	glTranslatef(glMoveX, glMoveY, 0.0f);

	glScalef(1.0f / lmax, 1.0f / lmax, 1.0f / lmax);
	
	//////////////////////////////////////////////////////////
	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition0);
	glLightfv(GL_LIGHT1, GL_POSITION, LightPosition1);
	glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, 1.0f);
	//////////////////////////////////////////////////////////

	for (int i = 0; i < 8; i++)
	{
		Vertex * vMax = &m_pMesh->m_pBB->m_vMax;
		Vertex * vMin = &m_pMesh->m_pBB->m_vMin;

		glBegin(GL_LINE_LOOP);
		//glVertex3f(m_ppChildNodes[i]->m_pMesh->m_pBB->m_vMin.x, m_ppChildNodes[i]->m_pMesh->m_pBB->m_vMin.y, m_ppChildNodes[i]->m_pMesh->m_pBB->m_vMin.z);
		//glVertex3f(m_ppChildNodes[i]->m_pMesh->m_pBB->m_vMax.x, m_ppChildNodes[i]->m_pMesh->m_pBB->m_vMin.y, m_ppChildNodes[i]->m_pMesh->m_pBB->m_vMin.z);
		//glVertex3f(m_ppChildNodes[i]->m_pMesh->m_pBB->m_vMax.x, m_ppChildNodes[i]->m_pMesh->m_pBB->m_vMax.y, m_ppChildNodes[i]->m_pMesh->m_pBB->m_vMin.z);
		//glVertex3f(m_ppChildNodes[i]->m_pMesh->m_pBB->m_vMin.x, m_ppChildNodes[i]->m_pMesh->m_pBB->m_vMin.y, m_ppChildNodes[i]->m_pMesh->m_pBB->m_vMin.z);
		
		glVertex3f(vMin->x, vMin->y, vMin->z);	//0
		glVertex3f(vMax->x, vMin->y, vMin->z);	//1
		glVertex3f(vMax->x, vMax->y, vMin->z);	//3
		glVertex3f(vMin->x, vMax->y, vMin->z);	//2
		
		glVertex3f(vMin->x, vMin->y, vMin->z);	//0
		glVertex3f(vMin->x, vMax->y, vMin->z);	//2
		glVertex3f(vMin->x, vMax->y, vMax->z);	//6
		glVertex3f(vMin->x, vMin->y, vMax->z);	//4

		glVertex3f(vMin->x, vMin->y, vMax->z);	//4
		glVertex3f(vMax->x, vMin->y, vMax->z);	//5
		glVertex3f(vMax->x, vMax->y, vMax->z);	//7
		glVertex3f(vMin->x, vMax->y, vMax->z);	//6

		glVertex3f(vMin->x, vMax->y, vMax->z);	//6
		glVertex3f(vMin->x, vMax->y, vMin->z);	//2
		glVertex3f(vMax->x, vMax->y, vMin->z);	//3
		glVertex3f(vMax->x, vMax->y, vMax->z);	//7

		glVertex3f(vMax->x, vMax->y, vMax->z);	//7
		glVertex3f(vMax->x, vMax->y, vMin->z);	//3
		glVertex3f(vMax->x, vMin->y, vMin->z);	//1
		glVertex3f(vMax->x, vMin->y, vMax->z);	//5

		glVertex3f(vMax->x, vMin->y, vMax->z);	//5
		glVertex3f(vMin->x, vMin->y, vMax->z);	//4
		glVertex3f(vMin->x, vMin->y, vMin->z);	//0
		glVertex3f(vMax->x, vMin->y, vMin->z);	//1
		glEnd();
	}

	switch (state) {
	case 1:
		glBegin(GL_POINTS);
		for (int Vindex = 0; Vindex < m_pMesh->m_numVerts; Vindex++) {
			glVertex3f(m_pMesh->m_pVertices[Vindex].x, m_pMesh->m_pVertices[Vindex].y, m_pMesh->m_pVertices[Vindex].z);
		}
		glEnd();
		break;

	case 2:
		glBegin(GL_LINES);
		for (int Tindex = 0; Tindex < m_pMesh->m_numFaces; Tindex++) {
			Triangle * pT = &m_pMesh->m_pFaces[Tindex];
			glVertex3f(m_pMesh->m_pVertices[pT->m_v[0]].x, m_pMesh->m_pVertices[pT->m_v[0]].y, m_pMesh->m_pVertices[pT->m_v[0]].z);
			glVertex3f(m_pMesh->m_pVertices[pT->m_v[1]].x, m_pMesh->m_pVertices[pT->m_v[1]].y, m_pMesh->m_pVertices[pT->m_v[1]].z);
			glVertex3f(m_pMesh->m_pVertices[pT->m_v[1]].x, m_pMesh->m_pVertices[pT->m_v[1]].y, m_pMesh->m_pVertices[pT->m_v[1]].z);
			glVertex3f(m_pMesh->m_pVertices[pT->m_v[2]].x, m_pMesh->m_pVertices[pT->m_v[2]].y, m_pMesh->m_pVertices[pT->m_v[2]].z);
			glVertex3f(m_pMesh->m_pVertices[pT->m_v[2]].x, m_pMesh->m_pVertices[pT->m_v[2]].y, m_pMesh->m_pVertices[pT->m_v[2]].z);
			glVertex3f(m_pMesh->m_pVertices[pT->m_v[0]].x, m_pMesh->m_pVertices[pT->m_v[0]].y, m_pMesh->m_pVertices[pT->m_v[0]].z);
		}
		glEnd();
		break;

	case 3:
		glBegin(GL_TRIANGLES);
		for (int Tindex = 0; Tindex < m_pMesh->m_numFaces; Tindex++) {
			Triangle * pT = &m_pMesh->m_pFaces[Tindex];
			for (int i = 0; i < 3; i++)
			{
				unsigned int Vi = pT->m_v[i];
				Vertex * pV = &m_pMesh->m_pVertices[Vi];
				glVertex3f(pV->x, pV->y, pV->z);
			}
		}
		glEnd();
	}
	glutPostRedisplay();
	glutSwapBuffers();
}

void OT_Node::DoKeyboard(unsigned char key, int x, int y)
{
	switch (key) {
	case '1':
		state = 1;	break;
	case '2':
		state = 2;	break;
	case '3':
		state = 3;	break;
	case 'w': //UP
		glMoveY += 1.0f;  break;
	case 's': //Down
		glMoveY -= 1.0f;  break;
	case 'a': //Left
		glMoveX -= 1.0f;  break;
	case 'd': //Right
		glMoveX += 1.0f;  break;
	case '+': //Scale Up
		glSize += 0.5f;  break;
	case '-': //Scale Down
		glSize -= 0.5f;	 break;
	case 'y': //Rotation X--
		glXAngle -= 10.0f;  break;
	case 'b': //Rotation X++
		glXAngle += 10.0f;  break;
	case 'g': //Rotation Y--
		glYAngle -= 10.0f;  break;
	case 'h': //Rotation Y++
		glYAngle += 10.0f;  break;
	case 'n': //Rotation Z--
		glZAngle -= 10.0f;  break;
	case 'm': //Rotation Z++
		glZAngle += 10.0f;  break;
	}
}
*/