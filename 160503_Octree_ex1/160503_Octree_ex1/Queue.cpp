#include "Queue.h"

Queue::Queue()
{
	pData = new OT_Node *[HS_SIZE];
	pTemp = NULL;
	front = 0;
	rear = 0;
}

Queue::~Queue()
{
	SAFEDELETE(pData);
	SAFEDELETE(pTemp);
}

bool Queue::empty() 
{
	if (front == rear)
		return true;
	else
		return false;
}

bool Queue::full()
{
	if ((front == 0) && (rear == HS_SIZE - 1))
		return true;
	else
		return false;
}

void Queue::Enqueue(OT_Node * a)
{
	if (full() == true)
		printf("\n >>> Queues are full!!");
	if (rear == HS_SIZE - 1)
		rear = 0;
	else
	{
		pData[rear] = a;
		rear = rear + 1;
		//printf("\n >>> rear ++!!");
	}
}

OT_Node * Queue::dequeue()
{	
	if ( empty() )
	{
		printf("\n >>> There are no data !!");
		return NULL;
	}
	else
	{
		int temp = front;

		if (front == HS_SIZE - 1)
			front = 0;
		else
		{
			front = front + 1;
			//printf("\n >>>> Dequeue_front >>>> %d", front);
		}
		return pData[temp];
	}
}