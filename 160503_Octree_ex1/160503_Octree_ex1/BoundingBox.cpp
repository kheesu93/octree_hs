#include <stdio.h>
#include "BoundingBox.h"


BoundingBox::BoundingBox(Vertex * pVerts, int numVerts)
{
	if (pVerts == NULL)
	{ 
		printf("\n Error! BoundingBox::BoundingBox(Vertex * pVerts, int numVerts) : null point received!");
		return;
	}
	makeBoundingBox(pVerts, numVerts);
	printf("\n1\n");
}

BoundingBox::BoundingBox()
{
}

BoundingBox::~BoundingBox()
{
}

void BoundingBox::CopyBB(BoundingBox BB)
{
	m_vMax = BB.m_vMax;
	m_vMin = BB.m_vMin;
}

void BoundingBox::CopyBB(BoundingBox *pBB)
{
	if (!pBB)
		return;
	CopyBB(*pBB);
}

void BoundingBox:: makeBoundingBox(Vertex * pVerts, int numVerts)
{
	m_vMax.x = FLT_MIN;		printf("m_vMax.x = %f\n", m_vMax.x);
	m_vMax.y = FLT_MIN;		printf("m_vMax.y = %f\n", m_vMax.y);
	m_vMax.z = FLT_MIN;		printf("m_vMax.z = %f\n", m_vMax.z);

	m_vMin.x = FLT_MAX;		printf("m_vMin.x = %f\n", m_vMin.x);
	m_vMin.y = FLT_MAX;		printf("m_vMin.y = %f\n", m_vMin.y);
	m_vMin.z = FLT_MAX;		printf("m_vMin.z = %f\n", m_vMin.z);

	for (int i=0; i<numVerts; i++)
	{
		///////////////	X_MAX, X_MIN
		if (pVerts[i].x > m_vMax.x)
			m_vMax.x = pVerts[i].x;
		if (pVerts[i].x < m_vMin.x)
			m_vMin.x = pVerts[i].x;

		///////////////	Y_MAX, Y_MIN
		if (pVerts[i].y > m_vMax.y)
			m_vMax.y = pVerts[i].y;
		if (pVerts[i].y < m_vMin.y)
			m_vMin.y = pVerts[i].y;

		///////////////	Z_MAX, Z_MIN
		if (pVerts[i].z > m_vMax.z)
			m_vMax.z = pVerts[i].z;
		if (pVerts[i].z < m_vMin.z)
			m_vMin.z = pVerts[i].z;
	}	

	printf("\n\n<makeBoundingBox>\nMax(%f, %f, %f)\nMin(%f, %f, %f)\n", m_vMax.x, m_vMax.y, m_vMax.z, m_vMin.x, m_vMin.y, m_vMin.z);
}

Vector3 BoundingBox:: getBBcentroid(void)
{
	//Vector3 m_v3Result;
	//m_v3Result_T = (0.0f, 0.0f, 0.0f);
	m_v3Result.x = 0.0f;
	m_v3Result.y = 0.0f;
	m_v3Result.z = 0.0f;

	m_v3Result.x = (m_vMax.x + m_vMin.x) / 2.0f;
	m_v3Result.y = (m_vMax.y + m_vMin.y) / 2.0f;
	m_v3Result.z = (m_vMax.z + m_vMin.z) / 2.0f;

	printf("\nBBcentroid m_v3Result(%f, %f, %f)\n",m_v3Result.x, m_v3Result.y, m_v3Result.z);

	return m_v3Result;
}

bool BoundingBox:: isInside(Vector3 p)
{
	if ((m_vMin.x <= p.x && p.x <= m_vMax.x) && (m_vMin.y <= p.y && p.y <= m_vMax.y) && (m_vMin.z <= p.z && p.z <= m_vMax.z))
		return true;
	else
		return false;
}