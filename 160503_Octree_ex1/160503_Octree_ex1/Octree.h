#ifndef ___OCTREE___
#define ___OCTREE___

#include "HScommon.h"
#include "OT_Node.h"
#include "Mesh.h"
#include "Queue.h"

class Octree
{
public:
	OT_Node *	m_pRoot;
	/*Vertex *	vMax;
	Vertex *	vMin;

	float lx;
	float ly;
	float lz;
	float lmax;*/

	Octree();
	Octree(Mesh * pMesh);
	~Octree();

	void BreadthTrav(OT_Node * pO);
	void draw();
};
#endif