#include <float.h>
#include "Mesh.h"
#include "Vertex.h"
//#define __HSDEBUG

Mesh::Mesh()
{
	m_pVertices = NULL;
	m_pFaces = NULL;
	m_pBB = NULL;
}

Mesh::~Mesh()
{
	SAFEDELETEARRAY(m_pVertices);
	SAFEDELETEARRAY(m_pFaces);
	SAFEDELETEARRAY(m_pBB);
}

Vector3 Mesh::getTcentroid(unsigned int Tid)
{
	Vector3 v3Result_T;
	//v3Result_T = (0.0f, 0.0f, 0.0f);
	v3Result_T.x = 0.0f;
	v3Result_T.y = 0.0f;
	v3Result_T.z = 0.0f;

	if (Tid > m_numFaces - 1)
	{
		printf("\nError : Tid is bigger  than the actual number of triangles.");
		return v3Result_T;
	}


	for (int i = 0; i < 3; i++)
	{
		v3Result_T.x += m_pVertices[m_pFaces[Tid].m_v[i]].x;
		v3Result_T.y += m_pVertices[m_pFaces[Tid].m_v[i]].y;
		v3Result_T.z += m_pVertices[m_pFaces[Tid].m_v[i]].z;
		//printf("%d %d %d\n", m_V[i].x, m_V[i].y, m_V[i].z);
	}
	v3Result_T.x /= 3.0f;
	v3Result_T.y /= 3.0f;
	v3Result_T.z /= 3.0f;

	//printf("%f\n%f\n%f\n\n", v3Result_T.x, v3Result_T.y, v3Result_T.z);
	return v3Result_T;
}

void Mesh::setDebug(bool bflag)
{
	m_debug = bflag;
}

void Mesh::debugOut(char *str, char *format)
{
	if (!m_debug)
		return;

	printf(str, format);
}

void Mesh:: meshRead(void)
{
	int tDummy;
	char format[100];
	char pro_x[100];

	std::string git_dir(getenv("GITDIR"));
	git_dir += "/Octree/160503_Octree_ex1/dog.ply";

	FILE * fp = fopen(git_dir.c_str(), "r");

	fscanf(fp, "%s\n", format);
	debugOut("%s\n", format);

	fgets(format, sizeof(format), fp);
	debugOut("%s", format);

	fgets(format, sizeof(format), fp);
	debugOut("%s", format);

	fscanf(fp, "%s %s %d\n", format, pro_x, &m_numVerts);

#ifdef __HSDEBUG	
	printf("%s %s %d\n", format, pro_x, m_numVerts);
#endif
	m_pVertices = new Vertex[m_numVerts];

	fgets(pro_x, sizeof(pro_x), fp);
	debugOut("%s", pro_x);
	fgets(pro_x, sizeof(pro_x), fp);
	debugOut("%s", pro_x);
	fgets(pro_x, sizeof(pro_x), fp);
	debugOut("%s", pro_x);
	fgets(pro_x, sizeof(pro_x), fp);
	debugOut("%s", pro_x);
	fgets(pro_x, sizeof(pro_x), fp);
	debugOut("%s", pro_x);
	fgets(pro_x, sizeof(pro_x), fp);
	debugOut("%s", pro_x);
	fgets(pro_x, sizeof(pro_x), fp);
	debugOut("%s", pro_x);

	fscanf(fp, "%s %s %d\n", format, pro_x, &m_numFaces);
#ifdef __HSDEBUG	
	printf("%s %s %d\n", format, pro_x, m_numFaces);
#endif
	m_pFaces = new Triangle[m_numFaces];

	fgets(pro_x, sizeof(pro_x), fp);
	debugOut("%s", pro_x);
	fgets(pro_x, sizeof(pro_x), fp);
	debugOut("%s", pro_x);

	for (int i = 0; i<m_numVerts; i++)
	{
		fscanf(fp, "%f %f %f %d %d %d %d", &m_pVertices[i].x, &m_pVertices[i].y, &m_pVertices[i].z, &m_pVertices[i].r, &m_pVertices[i].g, &m_pVertices[i].b, &m_pVertices[i].a);
#ifdef __HSDEBUG		
		printf("%f %f %f %d %d %d %d\n", m_pVertices[i].x, m_pVertices[i].y, m_pVertices[i].z, m_pVertices[i].r, m_pVertices[i].g, m_pVertices[i].b, m_pVertices[i].a);
#endif
	}
	
	for (int j = 0; j<m_numFaces; j++)
	{
		fscanf(fp, "%d %d %d %d", &tDummy, &m_pFaces[j].m_v[0], &m_pFaces[j].m_v[1], &m_pFaces[j].m_v[2]);

#ifdef __HSDEBUG		
		printf("%d %d %d %d\n", tDummy, m_pFaces[j].m_v[0], m_pFaces[j].m_v[1], m_pFaces[j].m_v[2]);
#endif
	}
	
	fclose(fp);

	m_pBB = new BoundingBox(m_pVertices, m_numVerts);
	printf("\n2\n");
}
