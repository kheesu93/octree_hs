#ifndef ______QUEUE___HH______
#define ______QUEUE___HH______

#include <iostream>
#define HS_SIZE 1 << 24

#include "HScommon.h"
#include "OT_Node.h"

class Queue
{
public:
	OT_Node ** pData;
	OT_Node * pTemp;
	int front;
	int rear;

	Queue();
	~Queue();

	void Enqueue(OT_Node * a);
	OT_Node * dequeue();

	bool empty();
	bool full();
};
#endif